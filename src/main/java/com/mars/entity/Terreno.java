package com.mars.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 *
 * @author DM
 */
@Entity
public class Terreno implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long terrenoId;
    
    public final int MIN_X = 0, MIN_Y = 0;
    public int maxX, maxY;
    
    private Integer x,y;
    private Character vista;

    public Integer getX() {
        return x;
    }
    private void setX(Integer x){
        this.x = x;
    }
    public Integer getY() {
        return y;
    }    
    private void setY(Integer y){
        this.y = y;
    }    
    public Character getVista() {
        return vista;
    }
    private void setVista(Character vista){
        this.vista = vista;
    }
    
    private void setMax(int x, int y){
        this.maxX = (x-1);
        this.maxY = (y-1);
    }
           
    public Terreno(Integer x, Integer y) {
       setMax(x,y);
       resetRobot();
    }
    
    public boolean resetRobot(){
        setX(0);
        setY(0);
        setVista('N');
        return true;
    }
    
    public void turnRight(){
        Character vistaAtual = getVista();
        switch(vistaAtual){
                case 'N': {
                    setVista('E');
                    break;
                }
                case 'E': {
                    setVista('S');
                    break;
                }
                case 'S': {
                    setVista('W');
                    break;
                }                
                case 'W': {
                    setVista('N');
                    break;
                }                
        }
    }
    
    public void turnLeft(){
        Character vistaAtual = getVista();
        switch(vistaAtual){
                case 'N': {
                    setVista('W');
                    break;
                }
                case 'W': {
                    setVista('S');
                    break;
                }
                case 'S': {
                    setVista('E');
                    break;
                }                
                case 'E': {
                    setVista('N');
                    break;
                }                
        }
    }
    
    public boolean move(){
        Character vistaAtual = getVista();
        switch(vistaAtual){
                case 'N': {
                    setY(getY()+1);
                    break;
                }
                case 'W': {
                    setX(getX()-1);
                    break;
                }
                case 'S': {
                    setY(getY()-1);
                    break;
                }                
                case 'E': {
                    setX(getX()+1);
                    break;
                }                
        }
        return  !(getX() < MIN_X || getX() > maxX || getY() < MIN_Y || getY() > maxY);
    } 
}