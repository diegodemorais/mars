package com.mars.controller;

import com.mars.entity.Terreno;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DM
 */
@RestController
public class TerrenoController {
    Terreno terreno;
    public TerrenoController(){
        terreno = new Terreno(5,5);
    }
    

    @RequestMapping(value = "/", produces = "application/json") //For both methods: POST and GET
    public ResponseEntity<String> index(HttpServletRequest request, HttpServletResponse response) {
        terreno.resetRobot(); //Setting initial okRequest
        return okRequest();
    }
    
    @RequestMapping(value = "/position", produces = "application/json") //For both methods: POST and GET
    public ResponseEntity<String> okRequest() {
        String content = ("(" + terreno.getX()+ ", " + terreno.getY() + ", " + terreno.getVista() + ")");
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(content,responseHeaders,HttpStatus.OK);
    }    
    
    @RequestMapping(value = "/error", produces = "application/json")
    public ResponseEntity<String> badRequest(){
        String content = "400 Bad Request";
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(content,responseHeaders,HttpStatus.BAD_REQUEST);
    }    
       
    @RequestMapping(value = "/{command}", produces = "application/json", method = RequestMethod.POST)
    public ResponseEntity<String> command(@PathVariable("command") String command) {
        boolean validPosition = terreno.resetRobot(); //Setting initial okRequest
        //boolean validPosition = true;
        CharacterIterator it = new StringCharacterIterator(command);
        for (Character ch = it.first() ; ch != CharacterIterator.DONE && validPosition; ch = it.next()) {
            switch(Character.toUpperCase(ch)) {
                case 'L': {
                    terreno.turnLeft();
                    break;
                }
                case 'R': {
                    terreno.turnRight();
                    break;
                }
                case 'M': {
                    validPosition = terreno.move();
                    break;
                }
                default:
                    return badRequest();
            }
        }
        if (validPosition)
            return okRequest();
        else
            return badRequest();
    }
}