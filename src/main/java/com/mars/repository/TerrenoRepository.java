package com.mars.repository;

import com.mars.entity.Terreno;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 *
 * @author DM
 */
@RepositoryRestResource(path = "terrenos", collectionResourceRel = "terrenos")
public interface TerrenoRepository extends JpaRepository<Terreno,Integer> {
    @RestResource(path = "by-id")
    Collection findByTerrenoId(@Param("tid") String tid);
}
